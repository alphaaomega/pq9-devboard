EESchema Schematic File Version 3
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lsd-kicad
LIBS:pq9-devboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PQ9 Testing Board"
Date "2017-12-08"
Rev "1.0"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PQ9-Connector PQ1
U 1 1 5A2AE350
P 3850 2250
F 0 "PQ1" V 3915 1772 50  0000 R CNN
F 1 "PQ9-Connector" V 3824 1772 50  0000 R CNN
F 2 "lsf-kicad-lib:PQ9" H 3850 2250 50  0001 C CNN
F 3 "" H 3850 2250 50  0001 C CNN
	1    3850 2250
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X09 P1
U 1 1 5A2AE50A
P 2950 2950
F 0 "P1" H 2869 2325 50  0000 C CNN
F 1 "CONN_01X09" H 2869 2416 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x09" H 2950 2950 50  0001 C CNN
F 3 "" H 2950 2950 50  0000 C CNN
	1    2950 2950
	-1   0    0    1   
$EndComp
$Comp
L CONN_02X09 P2
U 1 1 5A2AE6AE
P 3850 3750
F 0 "P2" V 3804 4228 50  0000 L CNN
F 1 "CONN_02X09" V 3895 4228 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09" H 3850 2550 50  0001 C CNN
F 3 "" H 3850 2550 50  0000 C CNN
	1    3850 3750
	0    1    -1   0   
$EndComp
Wire Wire Line
	3450 2450 3450 3500
Wire Wire Line
	3550 2450 3550 3500
Wire Wire Line
	3650 2450 3650 3500
Wire Wire Line
	3750 2450 3750 3500
Wire Wire Line
	3850 2450 3850 3500
Wire Wire Line
	3950 2450 3950 3500
Wire Wire Line
	4050 2450 4050 3500
Wire Wire Line
	4150 2450 4150 3500
Wire Wire Line
	3150 3350 3450 3350
Connection ~ 3450 3350
Wire Wire Line
	3150 3250 3550 3250
Connection ~ 3550 3250
Wire Wire Line
	3150 3150 3650 3150
Connection ~ 3650 3150
Wire Wire Line
	3150 3050 3750 3050
Connection ~ 3750 3050
Wire Wire Line
	3150 2950 3850 2950
Connection ~ 3850 2950
Wire Wire Line
	3150 2850 3950 2850
Connection ~ 3950 2850
Wire Wire Line
	3150 2750 4050 2750
Connection ~ 4050 2750
Wire Wire Line
	3150 2650 4150 2650
Connection ~ 4150 2650
Wire Wire Line
	3150 2550 4250 2550
Connection ~ 4250 2550
$Comp
L PQ9-Connector PQ2
U 1 1 5A2AEC30
P 6150 2250
F 0 "PQ2" V 6215 1772 50  0000 R CNN
F 1 "PQ9-Connector" V 6124 1772 50  0000 R CNN
F 2 "lsf-kicad-lib:PQ9" H 6150 2250 50  0001 C CNN
F 3 "" H 6150 2250 50  0001 C CNN
	1    6150 2250
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X09 P3
U 1 1 5A2AEC36
P 5250 2950
F 0 "P3" H 5169 2325 50  0000 C CNN
F 1 "CONN_01X09" H 5169 2416 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x09" H 5250 2950 50  0001 C CNN
F 3 "" H 5250 2950 50  0000 C CNN
	1    5250 2950
	-1   0    0    1   
$EndComp
$Comp
L CONN_02X09 P4
U 1 1 5A2AEC3C
P 6150 3750
F 0 "P4" V 6104 4228 50  0000 L CNN
F 1 "CONN_02X09" V 6195 4228 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09" H 6150 2550 50  0001 C CNN
F 3 "" H 6150 2550 50  0000 C CNN
	1    6150 3750
	0    1    -1   0   
$EndComp
Wire Wire Line
	5750 2450 5750 3500
Wire Wire Line
	5850 2450 5850 3500
Wire Wire Line
	5950 2450 5950 3500
Wire Wire Line
	6050 2450 6050 3500
Wire Wire Line
	6150 2450 6150 3500
Wire Wire Line
	6250 2450 6250 3500
Wire Wire Line
	6350 2450 6350 3500
Wire Wire Line
	6450 2450 6450 3500
Wire Wire Line
	6550 2450 6550 3500
Wire Wire Line
	5450 3350 5750 3350
Connection ~ 5750 3350
Wire Wire Line
	5450 3250 5850 3250
Connection ~ 5850 3250
Wire Wire Line
	5450 3150 5950 3150
Connection ~ 5950 3150
Wire Wire Line
	5450 3050 6050 3050
Connection ~ 6050 3050
Wire Wire Line
	5450 2950 6150 2950
Connection ~ 6150 2950
Wire Wire Line
	5450 2850 6250 2850
Connection ~ 6250 2850
Wire Wire Line
	5450 2750 6350 2750
Connection ~ 6350 2750
Wire Wire Line
	5450 2650 6450 2650
Connection ~ 6450 2650
Wire Wire Line
	5450 2550 6550 2550
Connection ~ 6550 2550
$Comp
L CONN_01X04 P6
U 1 1 5A2AEF89
P 8600 2700
F 0 "P6" H 8678 2741 50  0000 L CNN
F 1 "CONN_01X04" H 8678 2650 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 8600 2700 50  0001 C CNN
F 3 "" H 8600 2700 50  0000 C CNN
	1    8600 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2550 8000 2550
Text Label 8000 2550 0    60   ~ 0
RST
Wire Wire Line
	8400 2650 8000 2650
Wire Wire Line
	8400 2750 8000 2750
Wire Wire Line
	8400 2850 8000 2850
Text Label 8000 2650 0    60   ~ 0
D-
Text Label 8000 2750 0    60   ~ 0
D+
Text Label 8000 2850 0    60   ~ 0
GND
Wire Wire Line
	3450 4000 3450 4400
Text Label 3450 4400 1    60   ~ 0
RST
Wire Wire Line
	3550 4000 3550 4400
Wire Wire Line
	3650 4000 3650 4400
Wire Wire Line
	3750 4000 3750 4400
Text Label 3550 4400 1    60   ~ 0
D-
Text Label 3650 4400 1    60   ~ 0
D+
Text Label 3750 4400 1    60   ~ 0
GND
Wire Wire Line
	5750 4000 5750 4400
Text Label 5750 4400 1    60   ~ 0
RST
Wire Wire Line
	5850 4000 5850 4400
Wire Wire Line
	5950 4000 5950 4400
Wire Wire Line
	6050 4000 6050 4400
Text Label 5850 4400 1    60   ~ 0
D-
Text Label 5950 4400 1    60   ~ 0
D+
Text Label 6050 4400 1    60   ~ 0
GND
Wire Wire Line
	4250 4000 4250 4400
Text Label 4250 4400 1    60   ~ 0
GND
Wire Wire Line
	6550 4000 6550 4400
Text Label 6550 4400 1    60   ~ 0
GND
$Comp
L CONN_01X05 P7
U 1 1 5A2AF845
P 8600 3450
F 0 "P7" H 8678 3491 50  0000 L CNN
F 1 "CONN_01X05" H 8678 3400 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" H 8600 3450 50  0001 C CNN
F 3 "" H 8600 3450 50  0000 C CNN
	1    8600 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3250 8000 3250
Wire Wire Line
	8400 3350 8000 3350
Wire Wire Line
	8000 3450 8400 3450
Wire Wire Line
	8000 3550 8400 3550
Wire Wire Line
	8000 3650 8400 3650
Text Label 8000 3650 0    60   ~ 0
GND
Text Label 8000 3250 0    60   ~ 0
V1
Text Label 8000 3350 0    60   ~ 0
V2
Text Label 8000 3450 0    60   ~ 0
V3
Text Label 8000 3550 0    60   ~ 0
V4
Wire Wire Line
	6150 4000 6150 4400
Wire Wire Line
	6250 4000 6250 4400
Wire Wire Line
	6350 4400 6350 4000
Wire Wire Line
	6450 4400 6450 4000
Text Label 6150 4400 1    60   ~ 0
V1
Text Label 6250 4400 1    60   ~ 0
V2
Text Label 6350 4400 1    60   ~ 0
V3
Text Label 6450 4400 1    60   ~ 0
V4
Wire Wire Line
	3850 4000 3850 4400
Wire Wire Line
	3950 4000 3950 4400
Wire Wire Line
	4050 4400 4050 4000
Wire Wire Line
	4150 4400 4150 4000
Text Label 3850 4400 1    60   ~ 0
V1
Text Label 3950 4400 1    60   ~ 0
V2
Text Label 4050 4400 1    60   ~ 0
V3
Text Label 4150 4400 1    60   ~ 0
V4
$Comp
L CONN_01X02 P8
U 1 1 5A2AFDB4
P 8600 4100
F 0 "P8" H 8678 4141 50  0000 L CNN
F 1 "CONN_01X02" H 8678 4050 50  0000 L CNN
F 2 "lsf-kicad-lib:mkds_1,5-2" H 8600 4100 50  0001 C CNN
F 3 "" H 8600 4100 50  0000 C CNN
	1    8600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4050 8000 4050
Wire Wire Line
	8400 4150 8000 4150
Text Label 8000 4150 0    60   ~ 0
GND
Text Label 8000 4050 0    60   ~ 0
V
$Comp
L CONN_02X04 P5
U 1 1 5A2B00E6
P 8350 4800
F 0 "P5" H 8350 5165 50  0000 C CNN
F 1 "CONN_02X04" H 8350 5074 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04" H 8350 3600 50  0001 C CNN
F 3 "" H 8350 3600 50  0000 C CNN
	1    8350 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 4650 7700 4650
Wire Wire Line
	8100 4750 7700 4750
Wire Wire Line
	7700 4850 8100 4850
Wire Wire Line
	7700 4950 8100 4950
Text Label 7700 4650 0    60   ~ 0
V1
Text Label 7700 4750 0    60   ~ 0
V2
Text Label 7700 4850 0    60   ~ 0
V3
Text Label 7700 4950 0    60   ~ 0
V4
Wire Wire Line
	8600 4950 8750 4950
Wire Wire Line
	8750 4950 8750 4650
Wire Wire Line
	8600 4650 8900 4650
Connection ~ 8750 4650
Wire Wire Line
	8600 4750 8750 4750
Connection ~ 8750 4750
Wire Wire Line
	8600 4850 8750 4850
Connection ~ 8750 4850
Text Label 8900 4650 2    60   ~ 0
V
$Comp
L CONN_01X04 P9
U 1 1 5A2B06F5
P 10050 2700
F 0 "P9" H 10128 2741 50  0000 L CNN
F 1 "CONN_01X04" H 10128 2650 50  0000 L CNN
F 2 "lsf-kicad-lib:mkds_1,5-4" H 10050 2700 50  0001 C CNN
F 3 "" H 10050 2700 50  0000 C CNN
	1    10050 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2550 9450 2550
Text Label 9450 2550 0    60   ~ 0
RST
Wire Wire Line
	9850 2650 9450 2650
Wire Wire Line
	9850 2750 9450 2750
Wire Wire Line
	9850 2850 9450 2850
Text Label 9450 2650 0    60   ~ 0
D-
Text Label 9450 2750 0    60   ~ 0
D+
Text Label 9450 2850 0    60   ~ 0
GND
$Comp
L CONN_01X05 P10
U 1 1 5A2B07FF
P 10050 3450
F 0 "P10" H 10128 3491 50  0000 L CNN
F 1 "CONN_01X05" H 10128 3400 50  0000 L CNN
F 2 "lsf-kicad-lib:mkds_1,5-5" H 10050 3450 50  0001 C CNN
F 3 "" H 10050 3450 50  0000 C CNN
	1    10050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3250 9450 3250
Wire Wire Line
	9850 3350 9450 3350
Wire Wire Line
	9450 3450 9850 3450
Wire Wire Line
	9450 3550 9850 3550
Wire Wire Line
	9450 3650 9850 3650
Text Label 9450 3650 0    60   ~ 0
GND
Text Label 9450 3250 0    60   ~ 0
V1
Text Label 9450 3350 0    60   ~ 0
V2
Text Label 9450 3450 0    60   ~ 0
V3
Text Label 9450 3550 0    60   ~ 0
V4
Wire Wire Line
	4250 2450 4250 3500
$EndSCHEMATC
